package app

import (
	"html/template"
	"net/http"
	"strconv"

	"../sys"
	"../feed"
	"github.com/gorilla/mux"
)

// GetRssFeed returns stored feed
func GetRssFeed(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	category := feed.GetFeedCategoryByCategory(vars["category"])
	xml := feed.Feed(category, Accessor)
	writer.Header().Set("Content-Type", "text/html")
	writer.Write([]byte(xml))
}

// GetScrapeByID scrapes one particular category
func GetScrapeByID(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	id, _ := strconv.Atoi(vars["id"])
	selectedFeed := feed.GetFeedCategoryById(id)
	feedData := feed.Scrape(selectedFeed.Url)
	Accessor.Write(feedData, selectedFeed)
	writer.Write([]byte("Done for " + selectedFeed.Title))
}

// GetIndex returns available feeds
func GetIndex(response http.ResponseWriter, request *http.Request) {
	htmlTemplate, templateError := template.ParseFiles("views/index.html")
	sys.LogError(templateError)
	feeds := make([]string, 0)
	for _, value := range feed.CategoryDefaultLists {
		if feed.IndexFeed.FeedUrl != value.FeedUrl {
			link := "/feed/" + value.FeedUrl + "rss"
			feeds = append(feeds, link)
		}
	}
	templateError = htmlTemplate.Execute(response, feeds)
	sys.LogError(templateError)
}

// GetScrape scrapes all categories at once
func GetScrape(response http.ResponseWriter, request *http.Request) {
	scrapeAllTask()
	response.Write([]byte("Done for all feeds"))
}