package app

import (
	"../feed"
	"github.com/jasonlvhit/gocron"
	"os"
)

//Accessor to database
var Accessor = feed.NewDbAccessor(feed.DbConfig{"localhost", "web2rss", os.Getenv("WEB2RSS_DB_PASS"), "web2rss"})

//ScheduleScrape starts scheduler for article scraping
func ScheduleScrape() {
	gocron.Remove(scrapeAllTask)
	gocron.Clear()

	gocron.Every(2).Hours().Do(scrapeAllTask)
	<-gocron.Start()
}

func scrapeAllTask() {
	for _, category := range feed.CategoryDefaultLists {
		if feed.AllCombinedFeed.Url != category.Url {
			feedData := feed.Scrape(category.Url)
			Accessor.Write(feedData, category)
		}
	}
}
