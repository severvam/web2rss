package sys

import "log"

//LogError check and log error if needed
func LogError(err error) {
	if err != nil {
		log.Println(err)
	}
}
