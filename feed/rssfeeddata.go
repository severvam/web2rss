package feed

import "time"

type RssFeedData struct {
	Id             int
	Url            string
	Title          string
	CreatedDate    time.Time
	FeedCategoryId int
}

type RssFeedCategory struct {
	Id      int
	Url     string
	Title   string
	FeedUrl string
}

func (category RssFeedCategory) name() string {
	runes := []rune(category.FeedUrl)
	return string(runes[0:(len(runes)-1)])
}

func GetFeedCategoryByCategory(category string) RssFeedCategory {
	feed, exists := CategoryNameMaps[category]
	if !exists {
		feed = IndexFeed
	}
	return feed
}

func GetFeedCategoryById(id int) RssFeedCategory {
	feed, exists := CategoryDefaultLists[id];
	if !exists {
		feed = IndexFeed
	}
	return feed
}

var IndexFeed = RssFeedCategory{Id: 0, Url: "http://www.iflscience.com", Title: "Index", FeedUrl: ""}
var AllCombinedFeed = RssFeedCategory{Id: 1, Url: "http: //www.iflscience.com", Title: "All combined", FeedUrl: "all/"}

var environmentFeed = RssFeedCategory{Id: 2, Url: "http://www.iflscience.com/environment/", Title: "Environment", FeedUrl: "environment/"}
var technologyFeed = RssFeedCategory{Id: 3, Url: "http://www.iflscience.com/technology/", Title: "Technology", FeedUrl: "technology/"}
var spaceFeed = RssFeedCategory{Id: 4, Url: "http://www.iflscience.com/space/", Title: "Space", FeedUrl: "space/"}
var healthAndMedicineFeed = RssFeedCategory{Id: 5, Url: "http://www.iflscience.com/health-and-medicine/", Title: "Health and Medicine", FeedUrl: "health-and-medicine/"}
var brainFeed = RssFeedCategory{Id: 6, Url: "http://www.iflscience.com/brain/", Title: "Brain", FeedUrl: "brain/"}
var plantsAndAnimalsFeed = RssFeedCategory{Id: 7, Url: "http://www.iflscience.com/plants-and-animals/", Title: "Plants and animals", FeedUrl: "plants-and-animals/"}
var physicsFeed = RssFeedCategory{Id: 8, Url: "http://www.iflscience.com/physics/", Title: "Physics", FeedUrl: "physics/"}
var chemistryFeed = RssFeedCategory{Id: 9, Url: "http://www.iflscience.com/chemistry/", Title: "Chemistry", FeedUrl: "chemistry/"}
var editorsBlogFeed = RssFeedCategory{Id: 10, Url: "http://www.iflscience.com/editors-blog/", Title: "Editors blog", FeedUrl: "editors-blog/"}
var policyFeed = RssFeedCategory{Id: 11, Url: "http://www.iflscience.com/policy/", Title: "Policy", FeedUrl: "policy/"}

var CategoryDefaultLists = make(map[int]RssFeedCategory)

var CategoryNameMaps = make(map[string]RssFeedCategory)

func InitCategoryMaps() {
	CategoryDefaultLists[IndexFeed.Id] = IndexFeed
	CategoryDefaultLists[AllCombinedFeed.Id] = AllCombinedFeed
	CategoryDefaultLists[environmentFeed.Id] = environmentFeed
	CategoryDefaultLists[technologyFeed.Id] = technologyFeed
	CategoryDefaultLists[spaceFeed.Id] = spaceFeed
	CategoryDefaultLists[healthAndMedicineFeed.Id] = healthAndMedicineFeed
	CategoryDefaultLists[brainFeed.Id] = brainFeed
	CategoryDefaultLists[plantsAndAnimalsFeed.Id] = plantsAndAnimalsFeed
	CategoryDefaultLists[physicsFeed.Id] = physicsFeed
	CategoryDefaultLists[chemistryFeed.Id] = chemistryFeed
	CategoryDefaultLists[editorsBlogFeed.Id] = editorsBlogFeed
	CategoryDefaultLists[policyFeed.Id] = policyFeed

	CategoryNameMaps[IndexFeed.FeedUrl] = IndexFeed
	CategoryNameMaps[AllCombinedFeed.name()] = AllCombinedFeed
	CategoryNameMaps[environmentFeed.name()] = environmentFeed
	CategoryNameMaps[technologyFeed.name()] = technologyFeed
	CategoryNameMaps[spaceFeed.name()] = spaceFeed
	CategoryNameMaps[healthAndMedicineFeed.name()] = healthAndMedicineFeed
	CategoryNameMaps[brainFeed.name()] = brainFeed
	CategoryNameMaps[plantsAndAnimalsFeed.name()] = plantsAndAnimalsFeed
	CategoryNameMaps[physicsFeed.name()] = physicsFeed
	CategoryNameMaps[chemistryFeed.name()] = chemistryFeed
	CategoryNameMaps[editorsBlogFeed.name()] = editorsBlogFeed
	CategoryNameMaps[policyFeed.name()] = policyFeed
}
