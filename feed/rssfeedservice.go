package feed

import (
	"github.com/gorilla/feeds"
	"strconv"
)

func Feed(category RssFeedCategory, accessor *DbAccessor) string {
	feed := &feeds.Feed{
		Title:       "The Lighter Side of Science | IFLScience",
		Link:        &feeds.Link{Href: category.Url},
		Description: "RSS feed for IFLScience.com site.",
	}

	feedData := accessor.Read(category)
	items := make([]*feeds.Item, 0)
	for _, row := range feedData {
		item := &feeds.Item{
			Title:       row.Title,
			Link:        &feeds.Link{Href: row.Url},
			Description: "Category: " + GetFeedCategoryById(row.FeedCategoryId).Title + ". Title: " + row.Title,
			Id: strconv.Itoa(row.Id),
			Created:     row.CreatedDate,
		}
		items = append(items, item)
	}
	feed.Items = items
	rss, _ := feed.ToRss()
	return rss
}