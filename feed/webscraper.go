package feed

import (
	"github.com/PuerkitoBio/goquery"
	"log"
	"time"
	"../sys"
)

func Scrape(url string) map[string]RssFeedData {
	doc, err := goquery.NewDocument(url)
	sys.LogError(err)
	postMap := make(map[string]RssFeedData)

	doc.Find("article").Each(func(index int, node *goquery.Selection) {
		rssFeedData := RssFeedData{}
		node.Find("h2.title > a, a.article-link").Each(func(nodeIndex int, nodeElement *goquery.Selection) {
			rssFeedData.Title, _ = nodeElement.Attr("title")
			rssFeedData.Url, _ = nodeElement.Attr("href")
		})
		node.Find("div.inner > span.date, div.content > span.date").Each(func(nodeIndex int, nodeElement *goquery.Selection){
			data, _ :=  nodeElement.Html()
			rssFeedData.CreatedDate, _ = time.Parse("02 Jan 2006", data)
		})
		postMap[rssFeedData.Url] = rssFeedData
	})
	log.Println("Scraped articles: ", postMap)
	return postMap
}