package feed

import (
	"../sys"
	"database/sql"
	"strings"
	"strconv"
	_ "github.com/go-sql-driver/mysql"
)

type DbConfig struct {
	Host string
	User string
	Password string
	Database string
}

type DbAccessor struct {
	config DbConfig
}

func NewDbAccessor(config DbConfig) *DbAccessor {
	accessor := new(DbAccessor)
	accessor.config = config
	accessor.init()
	return accessor
}

func (accessor *DbAccessor) init() {
	db := openConnection(accessor.config)

	insert, err := db.Query("CREATE TABLE IF NOT EXISTS feed (id INT AUTO_INCREMENT primary key NOT NULL, url VARCHAR(1000), title VARCHAR(1000), createdDate DATE, feedCategoryId INT)")
	sys.LogError(err)

	defer insert.Close()
	defer db.Close()
}

func (accessor *DbAccessor) Write(feedMap map[string]RssFeedData, category RssFeedCategory) {
	db := openConnection(accessor.config)

	keys := make([]string, 0, len(feedMap))
	for key := range feedMap {
		keys = append(keys, "'" + key + "'")
	}
	selectQuery := "SELECT url FROM feed WHERE url in ( " + strings.Join(keys, ", ") + " )"
	rows, err := db.Query(selectQuery)
	sys.LogError(err)

	storedUrls := make(map[string]struct{})
	for rows.Next() {
		var url string
		err = rows.Scan(&url)
		sys.LogError(err)
		storedUrls[url] = struct{}{}
	}

	for key, value := range feedMap {
		_, inDatabase := storedUrls[key]
		if !inDatabase {
			db.Query("INSERT INTO feed (url,title,createdDate, feedCategoryId) VALUES (?,?,?,?)", value.Url, value.Title, value.CreatedDate, category.Id)
		}
	}

	defer db.Close()
}

func (accessor *DbAccessor) Read(category RssFeedCategory) []RssFeedData {
	db := openConnection(accessor.config)

	query := getQueryString(category)
	rows, err := db.Query(query)
	sys.LogError(err)

	feedDataList := make([]RssFeedData, 0)
	for rows.Next() {
		var feedData RssFeedData
		err = rows.Scan(&feedData.Id, &feedData.Url, &feedData.Title, &feedData.CreatedDate, &feedData.FeedCategoryId)
		sys.LogError(err)
		feedDataList = append(feedDataList, feedData)
	}

	defer db.Close()
	return feedDataList
}

func openConnection(conf DbConfig) *sql.DB {
	db, err := sql.Open("mysql", conf.User + ":" + conf.Password + "@tcp(127.0.0.1:3306)/" + conf.Database + "?parseTime=true")
	sys.LogError(err)
	return db
}

func getQueryString(category RssFeedCategory) string {
	if category.Id == AllCombinedFeed.Id {
		return "SELECT id, url, title, createdDate, feedCategoryId FROM feed ORDER BY createdDate DESC LIMIT 100"
	} else {
		return "SELECT id, url, title, createdDate, feedCategoryId FROM feed WHERE feedCategoryId = " + strconv.Itoa(category.Id) + " ORDER BY createdDate DESC LIMIT 100"
	}
}