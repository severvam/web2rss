package main

import (
	"log"
	"net/http"

	"os"

	"./app"
	"./feed"
	"github.com/gorilla/mux"
)

func main() {
	feed.InitCategoryMaps()
	logFile := setupLogger()
	defer logFile.Close()
	log.SetOutput(logFile)
	log.Println("Application starts")

	//app.ScheduleScrape()

	router := mux.NewRouter()
	router.HandleFunc("/", app.GetIndex).Methods("GET")
	router.HandleFunc("/scrape", app.GetScrape).Methods("GET")
	router.HandleFunc("/scrape/{id}", app.GetScrapeByID).Methods("GET")
	router.HandleFunc("/feed/{category}/rss", app.GetRssFeed).Methods("GET")
	router.HandleFunc("/feed/{category}/rss.xml", app.GetRssFeed).Methods("GET")

	router.HandleFunc("/favicon.ico", serveStatic)
	router.PathPrefix("/static").Handler(http.StripPrefix("/static", http.FileServer(http.Dir("./static"))))

	log.Fatal(http.ListenAndServe(":3000", router))
}

func setupLogger() *os.File {
	_ = os.Mkdir("logs/", 0700)
	file, err := os.OpenFile("logs/application.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	return file
}

func serveStatic(writer http.ResponseWriter, request *http.Request) {
	http.ServeFile(writer, request, "./static/favicon.ico")
}
